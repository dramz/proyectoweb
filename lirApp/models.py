from django.db import models

# Create your models here.
class Cliente(models.Model):
    TIPO_PROCEDIMIENTO = (
        ('LP', 'Liquidación Persona'),
        ('LE', 'Liquidación Empresa'),
        ('RP', 'Renegociación Persona'),
        ('RE', 'Reorganización Empresa'),
    )
    ESTADO = (
        (1, 'PENDIENTE'),
        (2, 'APROBADO'),
        (0, 'RECHAZADO'),
    )
    rut = models.CharField(max_length=25)
    nombre = models.CharField(max_length=255)
    apellidos = models.CharField(max_length=255)
    id_crm  = models.IntegerField(default=0, null=True, blank=True)
    tipo_procedimiento = models.CharField(max_length=2, choices=TIPO_PROCEDIMIENTO)
    estado  = models.IntegerField(default=1, choices=ESTADO)
    def __str__(self):
        return '%s %s %s' % (self.rut, self.nombre, self.tipo_procedimiento)

class Acreedor(models.Model):
    TIPO_ACREEDOR = (
        ('1', 'Banco de Chile'),
        ('2', 'Otros bancos'),
        ('3', 'Cajas'),
        ('4', 'Retail'),
        ('5', 'Otros'),
    )
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=255)
    tipo_acreedor = models.CharField(max_length=1, choices=TIPO_ACREEDOR)
    monto = models.IntegerField()
    moneda = models.CharField(max_length=1)
    def __str__(self):
        return '%s %s %s' % (self.cliente, self.nombre, self.tipo_acreedor)