from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('agregar_cliente', views.agregarCliente, name='agregarCliente'),
    path('clientes', views.ListaClientes.as_view(), name='listaClientes'),
    path('clientesAprobados', views.ListaClientesAprobados.as_view(), name='ClientesAprobados'),
    path('clientesRechazados', views.ListaClientesRechazados.as_view(), name='ClientesRechazados'),
    path('clientes/(?P<pk>\d+)$', views.ClienteDetalle, name='clienteDetalle'),
    path('clientes/aprobar/(?P<pk>\d+)$', views.ClienteAprobar, name='clienteAprobar'),
    path('clientes/rechazar/(?P<pk>\d+)$', views.ClienteRechazar, name='clienteRechazar'),
]