from django.apps import AppConfig


class LirappConfig(AppConfig):
    name = 'lirApp'
