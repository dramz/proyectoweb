from django.shortcuts import render
from django.views import generic
from .forms import ClienteForm
from .models import Cliente
from django.shortcuts import redirect
from django.http import HttpResponse
from django.http import Http404
from django.shortcuts import get_object_or_404

# Create your views here.


def index(request):
    return HttpResponse("<a href='http://dramz.pythonanywhere.com/lirApp/clientes'>listado de clientes<a/><a href='http://dramz.pythonanywhere.com/lirApp/agregar_cliente'>Agregar Cliente<a/>")

#def agregarCliente(request):
#    return HttpResponse("You're looking at question.")

#def agregarCliente(request):
#    form = ClienteForm()
#    return render(request, 'agregar_cliente.html', {'form': form})

def agregarCliente(request):
    if request.method == "POST":
        form = ClienteForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            #post.author = request.user
           # post.published_date = timezone.now()
            post.save()
            return redirect('listaClientes')
    else:
        form = ClienteForm()
    return render(request, 'agregar_cliente.html', {'form': form})

class ListaClientes(generic.ListView):
    model = Cliente
    context_object_name = 'lista_clientes'
    queryset = Cliente.objects.filter(estado=1)
    template_name = 'lista_clientes.html'


class ListaClientesAprobados(generic.ListView):
    model = Cliente
    context_object_name = 'lista_clientes'
    queryset = Cliente.objects.filter(estado=2 )
    template_name = 'lista_clientes_aprobados.html'

class ListaClientesRechazados(generic.ListView):
    model = Cliente
    context_object_name = 'lista_clientes'
    queryset = Cliente.objects.filter(estado=0 )
    template_name = 'lista_clientes_rechazados.html'

def ClienteDetalle(request,pk):
    try:
        cliente_id=Cliente.objects.get(pk=pk)
    except Cliente.DoesNotExist:
        raise Http404("Book does not exist")

    #book_id=get_object_or_404(Book, pk=pk)

    return render(
        request,
        'cliente_detalle.html',
        context={'cliente':cliente_id,}
    )
def ClienteAprobar(request,pk):
    try:
        cliente_id=Cliente.objects.get(pk=pk)
    except Cliente.DoesNotExist:
        raise Http404("Book does not exist")

    cliente_id=get_object_or_404(Cliente, pk=pk)
    #obj.name = "some_new_value"
    cliente_id.estado = 2

    cliente_id.save()
    return redirect('ClientesAprobados')
    #return render(
       # request,
        #'cliente_detalle.html',
        #context={'cliente':cliente_id,}
    #)
def ClienteRechazar(request,pk):
    try:
        cliente_id=Cliente.objects.get(pk=pk)
    except Cliente.DoesNotExist:
        raise Http404("Book does not exist")

    cliente_id=get_object_or_404(Cliente, pk=pk)
    #obj.name = "some_new_value"
    cliente_id.estado = 0

    cliente_id.save()
    return redirect('ClientesRechazados')