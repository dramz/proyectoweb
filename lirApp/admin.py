from django.contrib import admin
from .models import Cliente, Acreedor

# Register your models here.
admin.site.register(Cliente)
admin.site.register(Acreedor)